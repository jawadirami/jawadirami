<?php

namespace App\Http\Controllers;

use App\Mail\MailComment;
use App\Mail\SignupEmail;
use App\Models\Comment;
use App\Models\Page;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
  public function index()
  {

    return view("web.home");
  }


  public function home()
  {

    $company_id = "76078573";
    $client = new Client();
    $res = $client->request('GET', 'https://www.linkedin.com/pages-extensions/FollowCompany?id=' . $company_id . '&counter=bottom');

    $html = strval($res->getBody());
    $tag = "<div class=\"follower-count\">";
    $pos = strpos($html, $tag) + strlen($tag);
    $result = "";

    while ($html[$pos] != "<") {
      $result .= $html[$pos];
      $pos++;
    }
    $pourcentage = $result * 7 / 100;

    
    
      $articles = DB::table("articles")->join("pictures", "articles.id", "articles_id")->limit(3)->get();
      return view("web.home", ["articles" => $articles, "result" => $result, "pourcent" => $pourcentage]);
    
  }



  public function send_comment(Request $request)
  {
    $commentt = $request->comment;
    if (auth()->guest()) {
      return view('web.connexion.login');
    } else {
      $comment = new Comment();
      $comment->comment = $commentt;
      $comment->user_id = auth()->user()->id;
      $comment->save();
      $request->session()->put('comment', $commentt);
      Mail::send(new MailComment());
      return redirect()->back()->with(['success' => 'Nous avons recu votre avis!']);
    }
  }
}
