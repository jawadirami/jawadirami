<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->with("mailers.smtp")
            ->from('Contact@cpn-aide-aux-entreprises.com',"cpn-aide-aux-entreprises")
            ->to("ghaziarfaa@gmail.com")
            ->bcc("s.smida@jobid.fr")
            ->subject("Contact Us")
            ->markdown('web.mail.contact_mail');
    }
}
