import axios from "axios";

let baseApi = axios.create({
    headers: { 'content-type': 'application/json' },
    responseType:'json',
});

export default {
    api() {  
        let token = localStorage.getItem("token");
        if(token) baseApi.defaults.headers.common["Authorization"] = "Bearer "+token;
        baseApi.defaults.baseURL = "/api/";
        return baseApi;
    },
    siret(){
        return baseApi;
    },
};