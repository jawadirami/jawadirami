const axios = require("axios");
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const userID = urlParams.get('user')
document.addEventListener('DOMContentLoaded', function() {
  var calendarEl = document.getElementById('calendar');
  var calendar = new FullCalendar.Calendar(calendarEl, {
    initialView: 'dayGridMonth',
    locale:"fr",
    editable: true,
    selectable: true,

    selectConstraint: {
    },

    selectMirror: true,
    droppable: false,
    dayMaxEvents: true,
    weekends: true,
    headerToolbar: {
      left: "dayGridMonth",
      center: "title",
      right: "prev,next today",
    },
    buttonText: {
      today: "Aujourd'hui",
      month: "Mois",
      week: "Semaine",
      day: "Jour",
      list: "liste"
    },
    events:[
      {title:"hello",date:"2021-08-03", time:"12:30"}
    ],
    dateClick: function(info) {
      console.log(info)
      calendar.addEvent({title:info.dateStr,date:info.dateStr})
    },
    /* eventClick: function(info){
      info.event.remove()
    }, */
  });
  calendar.render();

  axios.create({
    baseURL: "http://localhost/api/",
    headers: { 'content-type': 'application/json' },
    responseType:'json',
  })
  .get("calendar/events/get")
  .then((response)=>{
    response.data.events.forEach(event => {
      calendar.addEvent({
        id:event.id,
        title:"Indisponible",
        date:event.date,
        color:"gray",
        extendedProps:{
          clientId:event.clients_id,
          time:event.time,
          description:event.description,
        }
      })
    });
  })
  .catch((error)=>console.log(error));
});


